package com.colinalworth.gwtdriver.by;

/*
 * #%L
 * gwt-driver
 * %%
 * Copyright (C) 2012 - 2013 Sencha Labs
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.colinalworth.gwtdriver.invoke.ClientMethodsFactory;
import com.colinalworth.gwtdriver.invoke.ExportedMethods;
import com.google.gwt.user.client.ui.Widget;

public class ByNearestWidget extends By {
	private final WebDriver driver;
	private final Class<?> widget;
	public ByNearestWidget(WebDriver driver) {
		this(driver, Widget.class);
	}
	public ByNearestWidget(WebDriver driver, Class<? extends Widget> type) {
		this.widget = type;
		this.driver = driver;
	}

	@Override
	public List<WebElement> findElements(SearchContext context) {
		WebElement elt = tryFindElement(context);
		if (elt != null) {
			return Collections.singletonList(elt);
		}
		return Collections.emptyList();

		//		do {
		//			String matches = (String) ((JavascriptExecutor)driver).executeAsyncScript("_"+moduleName+"_se.apply(this, arguments)", "instanceofwidget", elt, widget.getName());
		//			if ("true".equals(matches)) {
		//				System.out.println("...correct");
		//				return Collections.singletonList(elt);
		//			}
		//			List<WebElement>elts = elt.findElements(By.xpath("parent::node()[(not(html))]"));
		//			elt = elts.isEmpty() ? null : elts.get(0);
		//		} while (null != elt);
	}

	@Override
	public WebElement findElement(SearchContext context) {
		WebElement potentialElement = tryFindElement(context);
		if (potentialElement == null) {
			throw new NoSuchElementException("Cannot find a " + widget.getName() + " in " + context);
		}
		return potentialElement;
	}
	private WebElement tryFindElement(SearchContext context) {
		WebElement elt = context.findElement(By.xpath("."));
		ExportedMethods m = ClientMethodsFactory.create(ExportedMethods.class, driver);
		WebElement potentialElement = m.getContainingWidgetEltOfType(elt, widget.getName());
		return potentialElement;
	}

	@Override
	public String toString() {
		return "ByNearestWidget" + (widget == Widget.class ? "" : " " + widget.getName());
	}

}
